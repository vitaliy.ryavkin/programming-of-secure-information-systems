this book is largely concerned with hobbits, and from its pages a
reader may discover much of their character and a little of their
history. further information will also be found in the selection from
the red book of westmarch that has already been published, under
the title of the hobbit. that story was derived from the earlier chap-
ters of the red book, composed by bilbo himself, the first hobbit
to become famous in the world at large, and called by him there and
back again, since they told of his journey into the east and his return:
an adventure which later involved all the hobbits in the great events
of that age that are here related.
many, however, may wish to know more about this remarkable
people from the outset, while some may not possess the earlier book.
for such readers a few notes on the more important points are here
collected from hobbit-lore, and the first adventure is briefly recalled.
hobbits are an unobtrusive but very ancient people, more numer-
ous formerly than they are today; for they love peace and quiet and
good tilled earth: a well-ordered and well-farmed countryside was
their favourite haunt. they do not and did not understand or like
machines more complicated than a forge-bellows, a water-mill, or a
hand-loom, though they were skilful with tools. even in ancient days
they were, as a rule, shy of �the big folk�, as they call us, and now
they avoid us with dismay and are becoming hard to find. they are
quick of hearing and sharp-eyed, and though they are inclined to be
fat and do not hurry unnecessarily, they are nonetheless nimble and
deft in their movements. they possessed from the first the art of
disappearing swiftly and silently, when large folk whom they do not
wish to meet come blundering by; and this art they have developed
until to men it may seem magical. but hobbits have never, in fact,
studied magic of any kind, and their elusiveness is due solely to a
professional skill that heredity and practice, and a close friendship
with the earth, have rendered inimitable by bigger and clumsier races.
for they are a little people, smaller than dwarves: less stout and
stocky, that is, even when they are not actually much shorter. their
height is variable, ranging between two and four feet of our measure.